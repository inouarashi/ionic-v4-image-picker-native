# Ionic 4 Image Picker

## Setup

### Generate Ionic project

```
ionic start ionic-image-picker-app blank --type=angular
cd ionic-image-picker-app
```

### Install the camera library

```
ionic cordova plugin add cordova-plugin-camera
npm install @ionic-native/camera
```

### Install the file library

```
ionic cordova plugin add cordova-plugin-file
npm install @ionic-native/file
```

## Setup App

First, Prepare the App for Android

```
ionic cordova platform add android
```

Then, Modify `app.module.ts`

Import the module

```
import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
```

Add Camera and File module in providers
```
providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
],
```

In `home.page.ts`

```
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
```

Inject it in your Constructor (Dependency injection)

```
constructor(
	public actionSheetController: ActionSheetController,
	private camera: Camera,
	private file: File
) {}
```

Create a method

```
pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagePath = base64Image;
    }, (err) => {
      // Handle error
    });
}
```

Setup the Action Sheet

```
async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
}
```

## Resources

* [Camere Plugin](https://github.com/apache/cordova-plugin-camera)
* [File Plugin](https://github.com/apache/cordova-plugin-file)
* [Action Sheet Controller](https://ionicframework.com/docs/api/action-sheet-controller)
